<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace SM\Sales\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;

/**
 * @codeCoverageIgnore
 */
class UpgradeSchema implements UpgradeSchemaInterface {

    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        $installer = $setup;
        $installer->startSetup();
        if (version_compare($context->getVersion(), '0.1.1', '<')) {
            $this->addRetailDataToOrder($setup, $context);
        }
    }

    protected function addRetailDataToOrder(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        $installer = $setup;
        $installer->getConnection()->dropColumn($installer->getTable('quote'), 'outlet_id');
        $installer->getConnection()->dropColumn($installer->getTable('sales_order'), 'outlet_id');
        $installer->getConnection()->dropColumn($installer->getTable('sales_order_grid'), 'outlet_id');

        $installer->getConnection()->dropColumn($installer->getTable('quote'), 'retail_id');
        $installer->getConnection()->dropColumn($installer->getTable('sales_order'), 'retail_id');
        $installer->getConnection()->dropColumn($installer->getTable('sales_order_grid'), 'retail_id');

        $installer->getConnection()->dropColumn($installer->getTable('quote'), 'retail_status');
        $installer->getConnection()->dropColumn($installer->getTable('sales_order'), 'retail_status');
        $installer->getConnection()->dropColumn($installer->getTable('sales_order_grid'), 'retail_status');

        $installer->getConnection()->dropColumn($installer->getTable('quote'), 'retail_note');
        $installer->getConnection()->dropColumn($installer->getTable('sales_order'), 'retail_note');
        $installer->getConnection()->dropColumn($installer->getTable('sales_order_grid'), 'retail_note');

        $installer->getConnection()->dropColumn($installer->getTable('quote'), 'retail_has_shipment');
        $installer->getConnection()->dropColumn($installer->getTable('sales_order'), 'retail_has_shipment');
        $installer->getConnection()->dropColumn($installer->getTable('sales_order_grid'), 'retail_has_shipment');

        $installer->getConnection()->dropColumn($installer->getTable('quote'), 'is_exchange');
        $installer->getConnection()->dropColumn($installer->getTable('sales_order'), 'is_exchange');
        $installer->getConnection()->dropColumn($installer->getTable('sales_order_grid'), 'is_exchange');

        $installer->getConnection()->dropColumn($installer->getTable('quote'), 'user_id');
        $installer->getConnection()->dropColumn($installer->getTable('sales_order'), 'user_id');
        $installer->getConnection()->dropColumn($installer->getTable('sales_order_grid'), 'user_id');

        $installer->getConnection()->addColumn(
            $installer->getTable('quote'),
            'outlet_id',
            [
                'type'    => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                'comment' => 'Outlet id',
            ]
        );
        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order'),
            'outlet_id',
            [
                'type'    => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                'comment' => 'Outlet id',
            ]
        );
        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order_grid'),
            'outlet_id',
            [
                'type'    => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                'comment' => 'Outlet id',
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable('quote'),
            'retail_id',
            [
                'type'    => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length'  => '32',
                'comment' => 'Client id',
            ]
        );
        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order'),
            'retail_id',
            [
                'type'    => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length'  => '32',
                'comment' => 'Client id',
            ]
        );
        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order_grid'),
            'retail_id',
            [
                'type'    => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length'  => '32',
                'comment' => 'Client id',
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable('quote'),
            'retail_status',
            [
                'type'    => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                'comment' => 'Client Status',
            ]
        );
        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order'),
            'retail_status',
            [
                'type'    => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                'comment' => 'Client Status',
            ]
        );
        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order_grid'),
            'retail_status',
            [
                'type'    => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                'comment' => 'Client Status',
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable('quote'),
            'retail_note',
            [
                'type'    => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'comment' => 'Retail Note',
            ]
        );
        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order'),
            'retail_note',
            [
                'type'    => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'comment' => 'Retail Note',
            ]
        );
        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order_grid'),
            'retail_note',
            [
                'type'    => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'comment' => 'Retail Note',
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable('quote'),
            'retail_has_shipment',
            [
                'type'    => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                'comment' => 'Retail Shipment',
            ]
        );
        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order'),
            'retail_has_shipment',
            [
                'type'    => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                'comment' => 'Retail Shipment',
            ]
        );
        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order_grid'),
            'retail_has_shipment',
            [
                'type'    => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                'comment' => 'Retail Shipment',
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable('quote'),
            'is_exchange',
            [
                'type'    => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                'comment' => 'Retail Shipment',
            ]
        );
        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order'),
            'is_exchange',
            [
                'type'    => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                'comment' => 'Retail Shipment',
            ]
        );
        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order_grid'),
            'is_exchange',
            [
                'type'    => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                'comment' => 'Retail Shipment',
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable('quote'),
            'user_id',
            [
                'type'    => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'comment' => 'Cashier Id',
            ]
        );
        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order'),
            'user_id',
            [
                'type'    => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'comment' => 'Cashier Id',
            ]
        );
        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order_grid'),
            'user_id',
            [
                'type'    => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'comment' => 'Cashier Id',
            ]
        );
        $setup->endSetup();
    }
}